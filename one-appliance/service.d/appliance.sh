# ---------------------------------------------------------------------------- #
# Copyright 2018-2021, OpenNebula Project, OpenNebula Systems                  #
#                                                                              #
# Licensed under the Apache License, Version 2.0 (the "License"); you may      #
# not use this file except in compliance with the License. You may obtain      #
# a copy of the License at                                                     #
#                                                                              #
# http://www.apache.org/licenses/LICENSE-2.0                                   #
#                                                                              #
# Unless required by applicable law or agreed to in writing, software          #
# distributed under the License is distributed on an "AS IS" BASIS,            #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     #
# See the License for the specific language governing permissions and          #
# limitations under the License.                                               #
# ---------------------------------------------------------------------------- #

# Important notes #############################################################
#
# This appliance provide gitlab runner for either `docker`, `packer`
# or `shell` executor.
#
# Important notes #############################################################


# List of contextualization parameters
ONE_SERVICE_PARAMS=(
    'ONEAPP_GITLAB_RUNNER_NAME'  'configure' 'Name of the Runner'                     'M|text'
    'ONEAPP_GITLAB_URL'          'configure' 'URL of Gitlab'                          'M|text'
    'ONEAPP_GITLAB_TOKEN'        'configure' 'Gitlab secret token for the runner'     'M|password'
    'ONEAPP_GITLAB_EXECUTOR'     'configure' 'Executor type'                          'M|list'
    'ONEAPP_GITLAB_DOCKER_IMAGE' 'configure' 'Docker image used by “docker” executor' 'M|text'
    'ONEAPP_GITLAB_SHELL'        'configure' 'Shell to use for “Shell” executor'      'M|text'
    'ONEAPP_GITLAB_TAG_LIST'     'configure' 'Gitlab Runner Tags separated by comma'  'O|text'
    'ONEAPP_GITLAB_RUN_UNTAGGED' 'configure' 'Accept untagged jobs'                   'O|boolean'
    'ONEAPP_GITLAB_S3_ADDRESS'   'configure' 'Address of the S3 cache server'         'O|text'
    'ONEAPP_GITLAB_S3_ACCESSKEY' 'configure' 'Access Key to S3 storage'               'O|text'
    'ONEAPP_GITLAB_S3_SECRETKEY' 'configure' 'Secret key of S3 access'                'O|text'
    'ONEAPP_GITLAB_S3_BUCKET'    'configure' 'Bucket name'                            'O|text'
    'ONEAPP_GITLAB_S3_INSECURE'  'configure' 'S3 over HTTP without TLS'               'O|boolean'
)


### Appliance metadata ########################################################

ONE_SERVICE_NAME='Gitlab Runner appliance'
ONE_SERVICE_VERSION=0.1
ONE_SERVICE_BUILD=$(date +%s)
ONE_SERVICE_SHORT_DESCRIPTION='Appliance running Gitlab runner'
ONE_SERVICE_DESCRIPTION=$(cat <<EOF
Appliance running Gitlab runner.

It support different kind of executor:

- \`docker\`: the default with \`alpine:latest\` as default image used
- \`packer\`: is a \`shell\` executor with \`packer\` installed
- \`shell\`: use \`bash\` as default shell

An optional S3 caching can be enabled by setting the corresponding variables.

Initial configuration can be customized via parameters:

$(params2md 'configure')

EOF
)


### Contextualization defaults ################################################

ONEAPP_GITLAB_RUNNER_NAME=$(hostname -s)
ONEAPP_GITLAB_URL=''
ONEAPP_GITLAB_TOKEN=''
ONEAPP_GITLAB_EXECUTOR='docker'
ONEAPP_GITLAB_DOCKER_IMAGE='alpine:latest'
ONEAPP_GITLAB_SHELL='bash'
ONEAPP_GITLAB_TAG_LIST=''
ONEAPP_GITLAB_RUN_UNTAGGED='NO'
ONEAPP_GITLAB_S3_ADDRESS=''
ONEAPP_GITLAB_S3_ACCESSKEY=''
ONEAPP_GITLAB_S3_SECRETKEY=''
ONEAPP_GITLAB_S3_BUCKET=''
ONEAPP_GITLAB_S3_INSECURE=''

### Globals ###################################################################

DEP_PKGS="
    apt-transport-https
    ca-certificates
    coreutils
    curl
    jq
    openssh-server
    openssl
    pwgen
"


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


#
# service implementation
#

service_cleanup()
{
    :
}

service_install()
{
    # packages
    install_pkgs ${DEP_PKGS}

    # service metadata
    create_one_service_metadata

    # Install gitlab-runner
    cp "${ONE_SERVICE_SETUP_DIR}/apt/gitlab-runner.gpg" /etc/apt/trusted.gpg.d/
    cp "${ONE_SERVICE_SETUP_DIR}/apt/gitlab-runner.list" /etc/apt/sources.list.d/
    chmod 644 /etc/apt/trusted.gpg.d/gitlab-runner.gpg /etc/apt/sources.list.d/gitlab-runner.list
    apt update && apt -y install gitlab-runner
    systemctl stop gitlab-runner.service
    systemctl disable gitlab-runner.service

    # cleanup
    postinstall_cleanup

    msg info "INSTALLATION FINISHED"

    return 0
}

service_configure()
{
    local SHELL_OPTS=''

    ONEAPP_GITLAB_TAG_LIST="${ONEAPP_GITLAB_TAG_LIST},${ONEAPP_GITLAB_EXECUTOR}"

    cat > /etc/systemd/system/gitlab-runner-unregister.service <<EOF
[Unit]
Description=Unregister the gitlab-runner at shutdown
After=network-online.target
Conflicts=poweroff.target
DefaultDependencies=false

[Service]
Type=oneshot
ExecStart=/bin/true
ExecStop=/usr/bin/gitlab-runner unregister --all-runners
ExecStop=/bin/sh -c 'echo configure_success > /etc/one-appliance/status'
RemainAfterExit=yes
TimeoutSec=20

[Install]
WantedBy=multi-user.target
EOF
    chmod 644 /etc/systemd/system/gitlab-runner-unregister.service
    systemctl daemon-reload
    systemctl enable gitlab-runner.service gitlab-runner-unregister.service

    msg info "CONFIGURATION FINISHED"

    return 0
}

service_bootstrap()
{
    local _description="${ONEAPP_GITLAB_RUNNER_NAME} for executor ${ONEAPP_GITLAB_EXECUTOR}"
    local _executor="${ONEAPP_GITLAB_EXECUTOR}"
    local _tag_list="${ONEAPP_GITLAB_TAG_LIST},${ONEAPP_GITLAB_EXECUTOR}"
    local _shell_opts=''
    local _docker_opts=''
    local _cache_opts=''
    local _untagged_opt=''

    case "${ONEAPP_GITLAB_EXECUTOR}" in
        shell)
            _shell_opts="--shell ${ONEAPP_GITLAB_SHELL}"
            _tag_list="${_tag_list},${ONEAPP_GITLAB_SHELL}"
            _description="${_description}/${ONEAPP_GITLAB_SHELL}"
            ;;
        docker)
            install_pkgs 'docker.io' 'apparmor'
            _docker_opts="--docker-image ${ONEAPP_GITLAB_DOCKER_IMAGE}"
            _description="${_description}/${ONEAPP_GITLAB_DOCKER_IMAGE}"
            ;;
        packer)
            _executor='shell'
            cp "${ONE_SERVICE_SETUP_DIR}/apt/packer.gpg" /etc/apt/trusted.gpg.d/
            cp "${ONE_SERVICE_SETUP_DIR}/apt/packer.list" /etc/apt/sources.list.d/
            cp "${ONE_SERVICE_SETUP_DIR}/apt/opennebula.gpg" /etc/apt/trusted.gpg.d/
            cp "${ONE_SERVICE_SETUP_DIR}/apt/opennebula.list" /etc/apt/sources.list.d/
            chmod 644 /etc/apt/trusted.gpg.d/*.gpg /etc/apt/sources.list.d/*.list
            install_pkgs 'packer' 'qemu-system' 'libguestfs-tools' 'opennebula-libs'
            ;;
    esac

    if [[ -n "${ONEAPP_GITLAB_S3_ADDRESS}" \
       && -n "${ONEAPP_GITLAB_S3_ACCESSKEY}" \
       && -n "${ONEAPP_GITLAB_S3_SECRETKEY}" \
       && -n "${ONEAPP_GITLAB_S3_BUCKET}" ]]
    then
	_cache_opts="--cache-type s3
                     --cache-path jobs
                     --cache-shared
                     --cache-s3-server-address ${ONEAPP_GITLAB_S3_ADDRESS}
                     --cache-s3-access-key ${ONEAPP_GITLAB_S3_ACCESSKEY}
                     --cache-s3-secret-key ${ONEAPP_GITLAB_S3_SECRETKEY}
                     --cache-s3-bucket-name ${ONEAPP_GITLAB_S3_BUCKET}
                     ${ONEAPP_GITLAB_S3_INSECURE:+--cache-s3-insecure}
"
        _tag_list="${_tag_list},cache"
    fi

    if [[ "${ONEAPP_GITLAB_RUN_UNTAGGED}" = 'YES' \
       || -z "${_tag_list}" ]]
    then
        _untagged_opt="--run-untagged"
    else
	_untagged_opt="--run-untagged=false"
    fi

    gitlab-runner register \
                  --non-interactive \
                  --locked=false \
                  --access-level=not_protected \
                  --url "${ONEAPP_GITLAB_URL}" \
                  --registration-token "${ONEAPP_GITLAB_TOKEN}" \
                  --description "${_description}" \
                  --tag-list "${_tag_list}" \
                  --executor "${_executor}" \
                  ${_docker_opts} \
                  ${_shell_opts} \
                  ${_cache_opts} \
                  ${_untagged_opt}

    systemctl start gitlab-runner.service gitlab-runner-unregister.service

    msg info "BOOTSTRAP FINISHED"

    return 0
}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


#
# functions
#

postinstall_cleanup()
{
    msg info "Delete cache and stored packages"
    apt clean
}

install_pkgs()
{
    msg info "Install required packages"
    DEBIAN_FRONTEND=noninteractive
    apt update
    if ! apt -y install "${@}" ; then
        msg error "Package(s) installation failed"
        exit 1
    fi
}
