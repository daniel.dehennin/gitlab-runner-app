# Gitlab runner appliance

This OpenNebula appliance install
[gitlab-runner](https://docs.gitlab.com/runner/) on a Debian 11
machine.

A custom service unregister the runner when the virtual machine is shutdown.

## Configuration

Initial configuration can be customized via context parameters:

* `ONEAPP_GITLAB_RUNNER_NAME`: Name of the Runner
* `ONEAPP_GITLAB_URL`: URL of Gitlab
* `ONEAPP_GITLAB_TOKEN`: Gitlab secret token for the runner
* `ONEAPP_GITLAB_EXECUTOR`: Executor type, the value is added to `ONEAPP_GITLAB_TAG_LIST` when registering
  * `docker` the default one
  * `packer`
  * `shell`
* `ONEAPP_GITLAB_DOCKER_IMAGE`: Docker image used by `docker` executor
* `ONEAPP_GITLAB_SHELL`: Shell to use for "Shell" executor
* `ONEAPP_GITLAB_TAG_LIST`: Gitlab Runner Tags separated by comma

### Optional S3 caching server

You can configure the `gitlab-runner` to use an existing S3 server for caching:

* `ONEAPP_GITLAB_S3_ADDRESS`: Address of the S3 cache server
* `ONEAPP_GITLAB_S3_ACCESSKEY`: Key for S3 access
* `ONEAPP_GITLAB_S3_SECRETKEY`: Secret key for S3 access
* `ONEAPP_GITLAB_S3_BUCKET`: Bucket name
* `ONEAPP_GITLAB_S3_INSECURE`: Configure S3 over HTTP without TLS


## Virtual machine template

You need to define the following `USER_INPUTS` to your virtual machine:

```
  ONEAPP_GITLAB_URL = "M|text|URL of Gitlab| |",
  ONEAPP_GITLAB_TOKEN = "M|password|Gitlab secret token for the runner",
  ONEAPP_GITLAB_EXECUTOR = "M|list|Executor type|docker,packer,shell|docker",
  ONEAPP_GITLAB_RUNNER_NAME = "M|text|Name of the Runner| |",
  ONEAPP_GITLAB_DOCKER_IMAGE = "M|text|Docker image used by \"docker\" executor| |alpine:latest",
  ONEAPP_GITLAB_SHELL = "M|text|Shell to use for \"Shell\" executor| |bash",
  ONEAPP_GITLAB_TAG_LIST = "M|text|Gitlab Runner Tags separated by comma| |",
  ONEAPP_GITLAB_S3_ADDRESS = "O|text|Address of the S3 cache server| |",
  ONEAPP_GITLAB_S3_ACCESSKEY = "O|text|Key for S3 access| |",
  ONEAPP_GITLAB_S3_SECRETKEY = "O|password|Secret key for S3 access",
  ONEAPP_GITLAB_S3_BUCKET = "O|text|bucket name| |",
  ONEAPP_GITLAB_S3_INSECURE = "O|boolean|S3 over HTTP without TLS| |"
```

Note that the `packer` executor on KVM requires [nested guests](https://www.linux-kvm.org/page/Nested_Guests) with [CPU MODEL set to `host-passthrough`](https://docs.opennebula.io/6.2/management_and_operations/references/template.html?highlight=host%20passthrough#cpu-model-options-section).

## OneFlow service template

It's possible to create a service to deploy several runners in parallel, for example:

```
{
  "name": "gitlab-runner-debian-11",
  "deployment": "straight",
  "description": "",
  "roles": [
    {
      "name": "docker",
      "cardinality": 1,
      "vm_template": XXXX,
      "vm_template_contents": "NIC = [\n  NAME = \"_NIC0\",\n  NETWORK_ID = \"$Network\" ]\nDISK = [\n  IMAGE = \"gitlab-runner-debian-11\",\n  IMAGE_UNAME = \"gitlab\",\n  SIZE = \"51200\" ]\nTOKEN = \"YES\"\nONEAPP_GITLAB_RUNNER_NAME = \"$SERVICE_GITLAB_RUNNER_NAME\"\nONEAPP_GITLAB_URL = \"$SERVICE_GITLAB_URL\"\nONEAPP_GITLAB_TOKEN = \"$SERVICE_GITLAB_TOKEN\"\nONEAPP_GITLAB_EXECUTOR = \"docker\"\nONEAPP_GITLAB_TAG_LIST = \"$SERVICE_GITLAB_TAG_LIST\"\nONEAPP_GITLAB_S3_ADDRESS = \"$SERVICE_GITLAB_S3_ADDRESS\"\nONEAPP_GITLAB_S3_ACCESSKEY = \"$SERVICE_GITLAB_S3_ACCESSKEY\"\nONEAPP_GITLAB_S3_SECRETKEY = \"$SERVICE_GITLAB_S3_SECRETKEY\"\nONEAPP_GITLAB_S3_BUCKET = \"$SERVICE_GITLAB_S3_BUCKET\"\nONEAPP_GITLAB_S3_INSECURE = \"$SERVICE_GITLAB_S3_INSECURE\"\n ",
      "cooldown": 30,
      "elasticity_policies": [],
      "scheduled_policies": []
    },
    {
      "name": "packer",
      "cardinality": 1,
      "vm_template": XXXX,
      "vm_template_contents": "NIC = [\n  NAME = \"_NIC0\",\n  NETWORK_ID = \"$Network\" ]\nDISK = [\n  IMAGE = \"gitlab-runner-debian-11\",\n  IMAGE_UNAME = \"gitlab\",\n  SIZE = \"102400\" ]\nTOKEN = \"YES\"\nONEAPP_GITLAB_RUNNER_NAME = \"$SERVICE_GITLAB_RUNNER_NAME\"\nONEAPP_GITLAB_URL = \"$SERVICE_GITLAB_URL\"\nONEAPP_GITLAB_TOKEN = \"$SERVICE_GITLAB_TOKEN\"\nONEAPP_GITLAB_EXECUTOR = \"packer\"\nONEAPP_GITLAB_TAG_LIST = \"$SERVICE_GITLAB_TAG_LIST\"\nONEAPP_GITLAB_S3_ADDRESS = \"$SERVICE_GITLAB_S3_ADDRESS\"\nONEAPP_GITLAB_S3_ACCESSKEY = \"$SERVICE_GITLAB_S3_ACCESSKEY\"\nONEAPP_GITLAB_S3_SECRETKEY = \"$SERVICE_GITLAB_S3_SECRETKEY\"\nONEAPP_GITLAB_S3_BUCKET = \"$SERVICE_GITLAB_S3_BUCKET\"\nONEAPP_GITLAB_S3_INSECURE = \"$SERVICE_GITLAB_S3_INSECURE\"\n ",
      "cooldown": 30,
      "elasticity_policies": [],
      "scheduled_policies": []
    },
    {
      "name": "shell",
      "cardinality": 1,
      "vm_template": XXXX,
      "vm_template_contents": "NIC = [\n  NAME = \"_NIC0\",\n  NETWORK_ID = \"$Network\" ]\nDISK = [\n  IMAGE = \"gitlab-runner-debian-11\",\n  IMAGE_UNAME = \"gitlab\",\n  SIZE = \"51200\" ]\nTOKEN = \"YES\"\nONEAPP_GITLAB_RUNNER_NAME = \"$SERVICE_GITLAB_RUNNER_NAME\"\nONEAPP_GITLAB_URL = \"$SERVICE_GITLAB_URL\"\nONEAPP_GITLAB_TOKEN = \"$SERVICE_GITLAB_TOKEN\"\nONEAPP_GITLAB_EXECUTOR = \"shell\"\nONEAPP_GITLAB_TAG_LIST = \"$SERVICE_GITLAB_TAG_LIST\"\nONEAPP_GITLAB_S3_ADDRESS = \"$SERVICE_GITLAB_S3_ADDRESS\"\nONEAPP_GITLAB_S3_ACCESSKEY = \"$SERVICE_GITLAB_S3_ACCESSKEY\"\nONEAPP_GITLAB_S3_SECRETKEY = \"$SERVICE_GITLAB_S3_SECRETKEY\"\nONEAPP_GITLAB_S3_BUCKET = \"$SERVICE_GITLAB_S3_BUCKET\"\nONEAPP_GITLAB_S3_INSECURE = \"$SERVICE_GITLAB_S3_INSECURE\"\n ",
      "cooldown": 30,
      "elasticity_policies": [],
      "scheduled_policies": []
    }
  ],
  "ready_status_gate": false,
  "automatic_deletion": false,
  "networks": {
    "Network": "M|network|Network to access internet| |id:"
  },
  "custom_attrs": {
    "SERVICE_GITLAB_RUNNER_NAME": "O|text|Name of the Runner| |",
    "SERVICE_GITLAB_URL": "M|text|URL of Gitlab| |https://gitlab.mim-libre.fr",
    "SERVICE_GITLAB_TOKEN": "M|password|Gitlab secret token for the runner",
    "SERVICE_GITLAB_TAG_LIST": "M|text|Gitlab Runner Tags separated by comma| |eole,one",
    "SERVICE_GITLAB_S3_ADDRESS": "O|text|Address of the S3 cache server| |",
    "SERVICE_GITLAB_S3_ACCESSKEY": "O|text|Key for S3 access| |",
    "SERVICE_GITLAB_S3_SECRETKEY": "O|password|Secret key for S3 access",
    "SERVICE_GITLAB_S3_BUCKET": "O|text|S3 access bucket name| |",
    "SERVICE_GITLAB_S3_INSECURE": "O|boolean|S3 over HTTP without TLS| |NO"
  },
  "registration_time": 1638458928
}
```
